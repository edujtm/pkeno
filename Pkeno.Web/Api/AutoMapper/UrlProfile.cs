using Microsoft.AspNetCore.Http;
using AutoMapper;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Api.AutoMapper
{
    public class UrlProfile : Profile
    {
        public UrlProfile()
        {
            // TODO: Não fazer lógica de domínio no AutoMapper
            CreateMap<ShortUrl, UrlItemDTO>()
                .ForMember(
                    url => url.ShortURL, 
                    opt => opt.MapFrom((src, dst, _, context) => 
                    {
                        var request = context.Options.Items["Request"] as HttpRequest;
                        return string.Format(
                            "{0}://{1}/click/{2}",
                            request.Scheme,
                            request.Host,
                            src.Segment
                        );
                    })
                )
                .ForMember(
                    url => url.LongURL, 
                    opt => opt.MapFrom((src, dst, _, context) => context.Options.Items["Url"])
                );
        }
    }
}
