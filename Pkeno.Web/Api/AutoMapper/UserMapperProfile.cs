using AutoMapper;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Api.AutoMapper
{
    public class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            CreateMap<ProfileViewModel, Profile>();
            CreateMap<Profile, ProfileViewModel>();
        }
    }
}
