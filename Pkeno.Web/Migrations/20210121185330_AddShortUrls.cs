﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pkeno.Web.Migrations
{
    public partial class AddShortUrls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShortUrls",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    long_url = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    segment = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    added = table.Column<DateTime>(type: "datetime2", nullable: false),
                    num_of_clicks = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShortUrls", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShortUrls");
        }
    }
}
