using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Pkeno.Web.Services;

namespace Pkeno.Web.Controllers
{
    public class ClickController : Controller
    {
        private readonly IUrlManager _urlManager;

        public ClickController(IUrlManager urlManager)
        {
            _urlManager = urlManager;
        }

        public async Task<ActionResult> Click([FromRoute] string segment)
        {
            var stat = await _urlManager.ClickAsync(segment);
            return Redirect(stat.ShortUrl.LongUrl);
        }
    }
}
