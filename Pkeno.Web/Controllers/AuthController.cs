using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

using Pkeno.Web.Domain.DTO;
using Pkeno.Web.Data;
using Pkeno.Web.Services;


namespace Pkeno.Web.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly UserManager<PkenoUser> _userManager;
        private readonly IJwtManager _jwtManager;

        public AuthController(UserManager<PkenoUser> userManager, IJwtManager jwtManager)
        {
            _userManager = userManager;
            _jwtManager = jwtManager;
        }

        [HttpPost("login")]
        [ProducesResponseType(typeof(JwtToken), 200)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> Login([FromBody] LoginCommand command)
        {
            var user = await _userManager.FindByNameAsync(command.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, command.Password))
            {
                var token = _jwtManager.KeyForUsername(user.UserName); 
                return Ok(token);
            }

            return Unauthorized();
        }

        [HttpPost("sign-up")]
        [EnsurePasswordStrength]
        [EnsurePasswordConfirmationMatch]
        [ProducesResponseType(typeof(JwtToken), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        public async Task<IActionResult> SignUp([FromBody] SignUpCommand command)
        {
            var user = new PkenoUser()
            {
                UserName = command.UserName,
                Email = command.Email,
            };
            var userResult = await _userManager.CreateAsync(user, command.Password);
            if (!userResult.Succeeded)
            {
                foreach (var error in userResult.Errors)
                {
                    ModelState.AddModelError("UserName", error.Description);
                }
                return BadRequest(ModelState);
            }

            var token = _jwtManager.KeyForUsername(user.UserName);
            return Ok(token);
        }
    }
}
