using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

using Pkeno.Web.Services;
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Controllers
{
    [Authorize]
    [Route("api/urls")]
    [ApiController]
    public class UrlController : Controller
    {
        private readonly IUrlManager _urlManager;
        private readonly IMapper _mapper;

        public UrlController(IUrlManager urlManager, IMapper mapper)
        {
            _urlManager = urlManager;
            _mapper = mapper;
        }

        [HttpPost("shorten")]
        public async Task<IActionResult> Shorten(UrlCreate url)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var shortUrl = await _urlManager.ShortenUrlAsync(url.Url);
            var resultUrl = _mapper.Map<UrlItemDTO>(shortUrl, opt => 
            {
                opt.Items["Request"] = Request;
                opt.Items["Url"] = url.Url;
            });

            return Ok(resultUrl);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUrls()
        {
            var urls = await _urlManager.GetAllAsync();
            var urlDtos = urls.Select(url => {
               return _mapper.Map<UrlItemDTO>(url, opt => 
                {
                    opt.Items["Request"] = Request;
                    opt.Items["Url"] = url.LongUrl;
                });
            }).ToList();

            return Ok(urlDtos);
        }

        [HttpDelete("{segment}")]
        public async Task<IActionResult> DeleteUrl([FromRoute] string segment)
        {
            await _urlManager.DeleteUrlAsync(segment); 
            return Ok();
        }
    }
}
