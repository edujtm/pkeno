using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

using Pkeno.Web.Data;
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserManager<PkenoUser> _userManager;
        private readonly IMapper _mapper;

        public UserController(UserManager<PkenoUser> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpGet("profile")]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ProfileViewModel), 200)]
        public async Task<IActionResult> GetProfile()
        {
            var username = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return NotFound($"Não foi possível obter informações do usuário");
            }

            var profile = _mapper.Map<ProfileViewModel>(user);
            return Ok(profile);
        }

        [HttpPut("profile")]
        public async Task<IActionResult> SaveProfile([FromBody] ProfileViewModel model)
        {
            var username = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return NotFound($"Não foi possível obter o usuário");
            }

            user.Email = model.Email;
            user.Description = model.Description;
            user.Phone = model.Phone;

            await _userManager.UpdateAsync(user);
            return Ok();
        }
    }
}
