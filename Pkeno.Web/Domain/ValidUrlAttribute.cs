using System;
using System.ComponentModel.DataAnnotations;

namespace Pkeno.Web.Domain
{
    public class ValidUrlAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var url = (string) value;
            Uri validatedUri;
            var isValid = Uri.TryCreate(url, UriKind.Absolute, out validatedUri);

            if (!isValid) {
                return new ValidationResult("Url is invalid.");
            }

            return ValidationResult.Success;
        }
    }
}
