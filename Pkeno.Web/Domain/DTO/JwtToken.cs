using System;

namespace Pkeno.Web.Domain.DTO
{
    public class JwtToken
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
