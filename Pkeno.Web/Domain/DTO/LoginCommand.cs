using System.ComponentModel.DataAnnotations;

namespace Pkeno.Web.Domain.DTO
{
    public class LoginCommand
    {
        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
