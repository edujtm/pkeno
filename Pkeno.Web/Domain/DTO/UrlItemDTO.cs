
namespace Pkeno.Web.Domain.DTO
{
    public class UrlItemDTO
    {
        public string ShortURL { get; set; }
        public string LongURL { get; set; }
        public int NumOfClicks { get; set; }
    }
}
