using System.ComponentModel.DataAnnotations;

using Pkeno.Web.Domain;

namespace Pkeno.Web.Domain.DTO
{
    public class UrlCreate
    {
        [Required]
        [ValidUrl]
        public string Url { get; set; }
    }
}
