using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

using Pkeno.Web.Domain.Entities;

namespace Pkeno.Web.Domain.DTO
{
    public class Url 
    {
        [StringLength(500)]
        public string ShortURL { get; set; }

        [StringLength(500)]
        public string LongURL { get; set; }

        public int NumOfClicks { get; set; }

        public IEnumerable<Stat> Stats { get; set; }
    }
}
