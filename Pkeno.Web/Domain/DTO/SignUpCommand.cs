using System.ComponentModel.DataAnnotations;

namespace Pkeno.Web.Domain.DTO
{
    public class SignUpCommand
    {
        [Required]
        [RegularExpression("[^\\s]+", ErrorMessage = "Spaces are not allowed in the username")]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string PasswordConfirmation { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }
    }
}
