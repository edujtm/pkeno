using System.ComponentModel.DataAnnotations;

namespace Pkeno.Web.Domain.DTO
{
    public class ProfileViewModel
    {
        [EmailAddress]
        public string Email { get; set; }

        public string Phone { get; set; }

        [StringLength(50)]
        public string Description { get; set; }
    }
}
