using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Services;

namespace Pkeno.Web.Data
{
    public class UrlManager : IUrlManager
    {

        private readonly IPkenoAppContext _context;
        private readonly ISegmentManager _segmentManager;

        public UrlManager(IPkenoAppContext context, ISegmentManager segmentManager)
        {
            _context = context; 
            _segmentManager = segmentManager;
        }

        public async Task<List<ShortUrl>> GetAllAsync()
        {
            return await _context.ShortUrls.ToListAsync();
        }

        public async Task DeleteUrlAsync(string segment)
        {
            _context.Stats.RemoveRange(
                    _context.Stats.Where(stat => stat.ShortUrl.Segment == segment)
            );
            _context.ShortUrls.RemoveRange(
                _context.ShortUrls.Where(u => u.Segment == segment)
            );
            await _context.SaveChangesAsync();
        }

        public async Task<ShortUrl> ShortenUrlAsync(string url)
        {
            // Se já existe uma url armazenada
            var dbUrl = await _context.ShortUrls.Where(shortUrl => shortUrl.LongUrl == url).FirstOrDefaultAsync();
            if (dbUrl != null)
            {
                return dbUrl;
            }

            var segment = _segmentManager.NewSegment(); 
            if (segment == string.Empty)
            {
                throw new ArgumentException($"Couldnt't create shortUrl for {url}");
            }

            var shortUrl = new ShortUrl
            {
                Added = DateTime.Now,
                LongUrl = url,
                NumOfClicks = 0,
                Segment = segment
            };


            await _context.ShortUrls.AddAsync(shortUrl);
            await _context.SaveChangesAsync();
            return shortUrl;
        }

        public async Task<Stat> ClickAsync(string segment)
        {
            var url = await _context.ShortUrls.Where(u => u.Segment == segment).FirstOrDefaultAsync();
            if (url == null)
            {
                throw new ArgumentException("Url was not found!");
            }

            url.NumOfClicks = url.NumOfClicks + 1;

            var stat = new Stat
            {
                ClickDate = DateTime.Now,
                ShortUrl = url
            };

            await _context.Stats.AddAsync(stat);
            await _context.SaveChangesAsync(); 
            return stat;
        }
    }
}
