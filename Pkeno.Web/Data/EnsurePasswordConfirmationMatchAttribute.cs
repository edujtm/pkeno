using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Data
{
    public class EnsurePasswordConfirmationMatchAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var command = context.ActionArguments["command"] as SignUpCommand;
            if (command.Password != command.PasswordConfirmation)
            {
                context.ModelState.AddModelError("Password Confirmation", "Password confirmation doesn't match");
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}
