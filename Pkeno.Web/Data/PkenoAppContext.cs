using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using Pkeno.Web.Domain.Entities;

namespace Pkeno.Web.Data
{
    public class PkenoAppContext : IdentityDbContext<PkenoUser>, IPkenoAppContext
    {
        public PkenoAppContext(DbContextOptions<PkenoAppContext> options)
            : base(options) {}

        public DbSet<ShortUrl> ShortUrls { get; set; }
        public DbSet<Stat> Stats { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken token = default)
        {
            return base.SaveChangesAsync(token);
        }
    }
}
