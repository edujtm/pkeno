using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Pkeno.Web.Domain.Entities;

namespace Pkeno.Web.Data
{
    public interface IPkenoAppContext
    {
        DbSet<ShortUrl> ShortUrls { get; }
        DbSet<Stat> Stats { get; }
        Task<int> SaveChangesAsync(CancellationToken token = default);
    }
}
