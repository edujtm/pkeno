using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Identity;

using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Data
{
    public class EnsurePasswordStrengthFilter : IAsyncActionFilter
    {
        private readonly UserManager<PkenoUser> _userManager;

        public EnsurePasswordStrengthFilter(UserManager<PkenoUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var command = context.ActionArguments["command"] as SignUpCommand;
            var errors = new List<IdentityResult>();
            var sucessfulPassword = true;
            foreach (var validator in _userManager.PasswordValidators)
            {
                var result = await validator.ValidateAsync(_userManager, null, command.Password);
                if (!result.Succeeded)
                {
                    errors.Add(result);
                    sucessfulPassword = false;
                }
            }

            if (!sucessfulPassword)
            {
                foreach (var error in errors)
                {
                    foreach (var description in error.Errors)
                    {
                        context.ModelState.AddModelError("Password", error.ToString());
                    }
                }
                context.Result = new BadRequestObjectResult(context.ModelState);
            } else {
                await next();
            }
        }
    }
}
