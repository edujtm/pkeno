using Microsoft.AspNetCore.Mvc;

namespace Pkeno.Web.Data
{
    public class EnsurePasswordStrengthAttribute : TypeFilterAttribute
    {
        public EnsurePasswordStrengthAttribute() 
            : base(typeof(EnsurePasswordStrengthFilter)) {}
    }
}
