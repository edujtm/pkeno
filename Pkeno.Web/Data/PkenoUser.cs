using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Pkeno.Web.Data
{
    public class PkenoUser : IdentityUser
    {
        [PersonalData]
        public string Description { get; set; }

        [PersonalData]
        [RegularExpression("^(\\(11\\) [9][0-9]{4}-[0-9]{4})|(\\(1[2-9]\\) [5-9][0-9]{3}-[0-9]{4})|(\\([2-9][1-9]\\) [5-9][0-9]{3}-[0-9]{4})$", ErrorMessage = "Telefone Inválido")]
        public string Phone { get; set; }
    }
}
