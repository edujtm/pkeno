using System;
using System.Linq;

using Pkeno.Web.Services;

namespace Pkeno.Web.Data
{
    public class SegmentManager : ISegmentManager
    {
        private readonly IPkenoAppContext _context;

        public SegmentManager(IPkenoAppContext context)
        {
            _context = context;
        }

        public string NewSegment()
        {
            int i = 0;
            while (true)
            {
                string segment = Guid.NewGuid().ToString().Substring(0, 6);
                if (!_context.ShortUrls.Where(u => u.Segment == segment).Any())
                {
                    return segment;
                }
                if (i > 30)
                {
                    break;
                }
                i++;
            }
            return string.Empty;
        }
    }
}
