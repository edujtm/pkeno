using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Pkeno.Web.Data
{
    // Temporary class to test auth
    public static class SeedDB
    {
        public static void UseSeedData(this IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<PkenoAppContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<PkenoUser>>();
            context.Database.EnsureCreated();
            if (!context.Users.Any())
            {
                var user = new PkenoUser()
                {
                    Email = "test@gmail.com",
                    UserName = "test",
                    SecurityStamp = Guid.NewGuid().ToString()
                };
                userManager.CreateAsync(user, "Test@123");
            }
        }
    }
}
