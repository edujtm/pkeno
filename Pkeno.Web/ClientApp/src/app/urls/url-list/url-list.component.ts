import {
  Component, ViewChild, OnInit
} from '@angular/core';
import { tap } from 'rxjs/operators';

import { ModalComponent } from '../../modal/modal.component';
import { UrlsService } from '../../services/urls.service';
import { ModalService } from '../../services/modal.service';
import { ShortUrl } from '../../models/short-url.model';

@Component({
  selector: 'app-url-list',
  templateUrl: './url-list.component.html',
  styleUrls: ['./url-list.component.css'],
  providers: [ModalService]
})
export class UrlListComponent implements OnInit {
  urls: ShortUrl[] = [];
  isFetching = false;
  urlToDelete: ShortUrl;
  @ViewChild(ModalComponent, { static: true }) modal: ModalComponent;

  constructor(
    private urlsService: UrlsService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.isFetching = true;
    this.urlsService
      .fetchUrls()
      .pipe(tap(() => { this.isFetching = false; }))
      .subscribe(() => {});

    this.urlsService.shortenedUrls.subscribe((urls: Array<ShortUrl>) => {
      this.urls = urls;
    });
  }

  openModal() {
    this.modalService.setShowModal(true);
  }

  onDeleteAccepted() {
    this.urlsService.deleteUrl(this.urlToDelete);
  }

  onDismissModal() {
    this.urlToDelete = null;
  }

  onUrlDelete(url: ShortUrl) {
    this.urlToDelete = url;
    this.openModal();
  }
}
