import { 
  Component, ViewChild, ElementRef, OnInit,
  Renderer2
} from '@angular/core';
import { Router } from '@angular/router';

import { UrlsService } from '../../services/urls.service';
import { ShortUrl } from '../../models/short-url.model';

@Component({
  selector: 'app-url-shortener',
  templateUrl: './url-shortener.component.html',
  styleUrls: ['./url-shortener.component.css']
})
export class UrlShortenerComponent implements OnInit {
  url = null;
  resultUrl: ShortUrl = null;
  @ViewChild('urlForm', { static: true }) urlForm: ElementRef;
  @ViewChild('urlInput', { static: true }) urlInput: ElementRef;

  constructor(
    private urlsService: UrlsService,
    private router: Router,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
  }

  showAllUrls() {
    this.router.navigate(['/url-list']);
  }

  onShorten() {
    this.renderer.removeClass(this.urlForm.nativeElement, 'was-validated');
    if (this.isValidUrl(this.url)) {
      this.renderer.addClass(this.urlInput.nativeElement, 'is-valid');
      this.urlsService
        .shorten(this.url)
        .subscribe((response: ShortUrl) => {
          console.log(`received response: ${JSON.stringify(response)}`);
          this.url = '';
          this.resultUrl = response;
        });
    } else {
      console.log(`Invalid url: ${this.url}`);
      this.renderer.addClass(this.urlInput.nativeElement, 'is-invalid');
    }
  }

  private isValidUrl(url: string) {
    let validated_url: URL;

    try {
      validated_url = new URL(url);
    } catch (_) {
      return false;
    }

    return validated_url.protocol === "http:" || validated_url.protocol === "https:";
  }
}
