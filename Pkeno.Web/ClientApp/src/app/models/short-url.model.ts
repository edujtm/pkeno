

export class ShortUrl {
  constructor(
    public shortUrl: string,
    public longUrl: string,
    public numOfClicks: number
  ) {}
}
