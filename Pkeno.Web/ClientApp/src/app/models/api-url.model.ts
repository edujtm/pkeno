

export interface ApiUrl {
  shortURL: string;
  longURL: string;
  numOfClicks: number;
}
