

export class AccountProfile {
  constructor(
    public email: string,
    public phone: string,
    public description: string
  ) {}
}
