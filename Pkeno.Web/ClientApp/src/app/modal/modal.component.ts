import {
  Component, ViewChild, ElementRef, OnInit,
  AfterViewInit, Input, Output, EventEmitter
} from '@angular/core';
import * as $ from 'jquery';
import 'bootstrap';

import { ModalService } from '../services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, AfterViewInit {
  @Input() modalTitle: string;
  @Input() message: string;
  @Input() buttonLabel: string;
  @Output() deleteAction: EventEmitter<void>;
  @Output() dismissAction: EventEmitter<void>;
  @ViewChild('exampleModal', { static: true }) modal: ElementRef;

  constructor(private modalService: ModalService) {
    this.deleteAction = new EventEmitter<void>();
    this.dismissAction = new EventEmitter<void>();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.modalService.isShowing
      .subscribe((isShowing) => {
        console.log(`is showing: ${isShowing}`);
        if (isShowing) {
          this.showModal();
        } else {
          this.hideModal();
        }
      });
  }

  sendModal() {
    this.deleteAction.emit();
    this.dismiss();
  }

  dismiss() {
    this.dismissAction.emit();
    this.modalService.setShowModal(false);
  }

  showModal() {
    $(this.modal.nativeElement).modal('show');
  }

  hideModal() {
    $(this.modal.nativeElement).modal('hide');
  }

  toggle() {
    const currentShowStatus = this.modalService.isShowing.value;
    this.modalService.setShowModal(!currentShowStatus);
  }
}
