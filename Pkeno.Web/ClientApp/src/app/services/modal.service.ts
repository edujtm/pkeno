import { BehaviorSubject } from 'rxjs';
import { Injectable } from "@angular/core";

@Injectable()
export class ModalService {
  public isShowing = new BehaviorSubject<boolean>(false);

  setShowModal(showModal: boolean) {
    this.isShowing.next(showModal);
  }
}
