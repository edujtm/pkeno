import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AccountProfile } from '../models/account-profile.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getProfile() : Observable<AccountProfile> {
    return this.http.get<{email: string, phone: string, description: string}>(
      '/api/user/profile'
    ).pipe(
      map(response => {
        return new AccountProfile(response.email, response.phone, response.description)
      })
    );
  }

  saveProfile(profile: AccountProfile) : Observable<void> {
    var putData = { ...profile };
    return this.http.put<void>(
      '/api/user/profile',
      putData
    )
  }
}
