import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

import { ShortUrl } from '../models/short-url.model';
import { ApiUrl } from '../models/api-url.model';

@Injectable()
export class UrlsService {
  readonly baseUrl = 'https://localhost:5001';
  _shortenedUrls: ShortUrl[] = [];
  shortenedUrls = new EventEmitter<ShortUrl[]>();

  constructor(
    private http: HttpClient
  ) {}

  shorten(url: string) {
    return this.http.post(
      `${this.baseUrl}/api/urls/shorten`,
      { url }
    ).pipe<ShortUrl>(map((response: ApiUrl) => {
      return new ShortUrl(response.shortURL, response.longURL, response.numOfClicks);
    })).pipe(tap((shortUrl: ShortUrl) => {
      this._shortenedUrls.push(shortUrl);
      this.shortenedUrls.emit(this._shortenedUrls);
    }));
  }

  fetchUrls() {
    return this.http.get(
      `${this.baseUrl}/api/urls/`
    ).pipe<ShortUrl[]>(map((response: ApiUrl[]) => {
      return response.map((apiUrl) => { 
        return new ShortUrl(
          apiUrl.shortURL, 
          apiUrl.longURL, 
          apiUrl.numOfClicks
        ) 
      });
    })).pipe(tap((response: ShortUrl[]) => {
      this._shortenedUrls = response;
      this.shortenedUrls.emit(this._shortenedUrls);
    }));
  }

  deleteUrl(url: ShortUrl) {
    const segment = url.shortUrl.split('/').pop();
    this.http.delete(
      `${this.baseUrl}/api/urls/${segment}`
    ).subscribe(() => {
      this._shortenedUrls = this._shortenedUrls.filter((u) => u.shortUrl !== url.shortUrl);
      this.shortenedUrls.emit(this._shortenedUrls);
    });
  }
}
