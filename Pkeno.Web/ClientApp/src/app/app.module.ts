import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { UrlShortenerComponent } from './urls/url-shortener/url-shortener.component';
import { UrlListComponent } from './urls/url-list/url-list.component';
import { ModalComponent } from './modal/modal.component';
import { LoginComponent } from './accounts/login/login.component';
import { RegisterComponent } from './accounts/register/register.component';
import { ProfileComponent } from './accounts/profile/profile.component';
import { AuthService } from './services/auth.service'
import { AuthGuard } from './guards/auth.guard';

import { UrlsService } from './services/urls.service';
import { UserService } from './services/user.service';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    UrlShortenerComponent,
    UrlListComponent,
    ModalComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter, 
        allowedDomains: ['localhost:4000'],
        disallowedRoutes: [
          'localhost:4000/api/auth/login',
          'localhost:4000/api/auth/sign-up',
        ]
      }
    }),
    RouterModule.forRoot([
      { path: '', component: UrlShortenerComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'url-list', component: UrlListComponent, canActivate: [AuthGuard] },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'sign-up', component: RegisterComponent }
    ])
  ],
  providers: [
    UrlsService, 
    AuthService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
