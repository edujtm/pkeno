import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';

import { UserService } from '../../services/user.service';
import { AccountProfile } from '../../models/account-profile.model';

export enum ResponseState {
  SUCCESS,
  ERROR,
  NONE,
};

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public State = ResponseState;
  public notificationText: string;
  public phone: string;
  public description: string;
  public email: string;
  public isFetching = false;
  public responseState = ResponseState.NONE;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getProfile()
      .subscribe(profile => {
        this.email = profile.email;
        this.phone = profile.phone;
        this.description = profile.description;
      });
  }

  submit() {
    var profile = new AccountProfile(
      this.email,
      this.phone,
      this.description
    );
    this.isFetching = true;
    this.userService.saveProfile(profile)
      .pipe(tap(_ => this.isFetching = false))
      .subscribe({
        complete: () => {
          // TODO: reset validation
          console.log("Sucessful update");
          this.notificationText = 'Sucesso ao salvar operação';
          this.responseState = ResponseState.SUCCESS;
        },
        error: (err) => {
          console.log(err);
          this.notificationText = 'Aconteceu um erro';
          this.responseState = ResponseState.ERROR;
        }
      })
  }
}
