import { 
  Component, ViewChild, ElementRef, OnInit,
  Renderer2
} from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { first, tap, catchError } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { AccountCreate } from '../../models/account-create.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public error: string;
  public username: string;
  public password: string;
  public passwordConfirmation: string;
  public email: string;
  @ViewChild('registerForm', { static: true }) registerForm: ElementRef;

  constructor(
    private auth: AuthService, 
    private router: Router,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
  }

  submit() {
    this.renderer.addClass(this.registerForm.nativeElement, 'was-validated');
    var accountData = new AccountCreate(
      this.username,
      this.email,
      this.password,
      this.passwordConfirmation
    );

    this.auth.signUp(accountData)
      .pipe(
        first(),
        tap(_ => this.renderer.removeClass(this.registerForm.nativeElement, 'was-validated')),
      ).subscribe(
        _ => this.router.navigate(['/']),
        _ => this.error = 'Aconteceu algum erro.'
      );
  }
}
