using System.Collections.Generic;
using System.Threading.Tasks;

using Pkeno.Web.Domain.Entities;

namespace Pkeno.Web.Services
{
    public interface IUrlManager
    {
        public Task<ShortUrl> ShortenUrlAsync(string url);
        public Task<Stat> ClickAsync(string url);
        public Task<List<ShortUrl>> GetAllAsync();
        public Task DeleteUrlAsync(string segment);
    }
}
