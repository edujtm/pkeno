
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Web.Services
{
    public interface IJwtManager
    {
        public JwtToken KeyForUsername(string username);
    }
}
