FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine


ENV JwtSettings__Key VP-6MI8NwUWsHZb0dSPqFstRSXe02CmAlcPLjCC9oVs


ENV ALPINE_MIRROR "http://dl-cdn.alpinelinux.org/alpine"
RUN echo "${ALPINE_MIRROR}/edge/main" >> /etc/apk/repositories
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ nodejs-current
RUN apk add --update npm
RUN node --version
