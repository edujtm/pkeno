using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Pkeno.Tests.Utils
{
    public class TestDbSet<T> : DbSet<T>, IQueryable, IEnumerable<T>,
           IAsyncEnumerable<T> where T : class
    {
        ObservableCollection<T> _data;
        IQueryable _query;

        public TestDbSet()
        {
            _data = new ObservableCollection<T>();
            _query = _data.AsQueryable();
        }

        public override EntityEntry<T> Add(T item)
        {
            _data.Add(item);
            return null;
        }

        public override EntityEntry<T> Remove(T item)
        {
            _data.Remove(item);
            return null;
        }

        public override EntityEntry<T> Attach(T item)
        {
            return null;
        }

        public override async ValueTask<EntityEntry<T>> AddAsync(T item, CancellationToken token)
        {
            _data.Add(item);
            return await ValueTask.FromResult(null as EntityEntry<T>);
        }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken token)
        {
            return new TestDbAsyncEnumerator<T>(_data.GetEnumerator());
        }
    }
}
