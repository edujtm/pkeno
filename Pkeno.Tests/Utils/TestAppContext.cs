using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using Pkeno.Web.Data;
using Pkeno.Web.Domain.Entities;

namespace Pkeno.Tests.Utils
{
    public class TestPkenoAppContext : IdentityDbContext<PkenoUser>, IPkenoAppContext
    {

        public TestPkenoAppContext(DbSet<ShortUrl> shortUrlsSet, DbSet<Stat> statsSet)
        {
            this.ShortUrls = shortUrlsSet; 
            this.Stats = statsSet;
        }

        public DbSet<ShortUrl> ShortUrls { get; set; }
        public DbSet<Stat> Stats { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken token)
        {
            return await Task.FromResult(0);
        }
    }
}
