using System;
using Xunit;

using Pkeno.Web.Controllers;
using Pkeno.Web.Domain.DTO;

namespace Pkeno.Tests.Setup
{
    /// Testes apenas para auxilio no setup das classes,
    /// auxiliando na conformidade do código com os testes
    ///
    /// Normalmente não se testam estas coisas.
    public class InitialTest
    {
        /// Teste simples apenas para garantir que o nome
        /// do controlador está de acordo com os testes
        [Fact]
        public void Controller_ShouldBeNamed_UrlController()
        {
            Assert.Equal(nameof(UrlController), "UrlController");
        }

        // Teste simples para garante que o nome da ação
        // do controlador está de acordo com os testes
        [Fact]
        public void ControllerAction_ShouldBeNamed_Shorten()
        {
            Assert.Equal(nameof(UrlController.Shorten), "Shorten");
        }

        // Garante que os campos do modelo estejam de acordo
        // com o que eu defini nos testes
        [Fact]
        public void UrlModel_ShouldHave_ShortAndLongComponents()
        {
            var url = new Url {
                LongURL = "http://www.google.com/",
                ShortURL = "http://pkeno.io/a1b2c3",
            };
        }
    }
}
