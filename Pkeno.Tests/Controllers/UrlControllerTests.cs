using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using Xunit;
using Moq;

using Pkeno.Web.Api.AutoMapper;
using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Domain.DTO;
using Pkeno.Web.Services;
using Pkeno.Web.Controllers;

namespace Pkeno.Tests.Controllers
{
    public class UrlControllerTests
    {

        private readonly IMapper _mapper;

        public UrlControllerTests()
        {
            var profile = new UrlProfile();
            var conf = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            _mapper = new Mapper(conf);
        }

        // Este teste verifica se a injeção de dependências foi feita
        // de forma correta.
        //
        // Para conseguir fazer isso, é necessário declarar uma classe
        // que implementa a interface IURLManager e então cadastrá-la
        // no container de DI utilizando services.AddScoped<T>() em
        // ConfigureServices da classe Startup
        [Fact]
        public void UrlController_ShouldCall_IURLManager_ShortenUrl()
        {

            var mock = new Mock<IUrlManager>();
            var controller = new UrlController(mock.Object, _mapper);

            var url = "https://www.google.com/";
            var urlCreate = new UrlCreate { Url = url };
            var result = controller.Shorten(urlCreate);

            mock.Verify(urlManager => urlManager.ShortenUrlAsync(url), Times.Once());
        }

        /// Testa se o argumento nulo será passado para o encurtador
        /// caso a url não seja especificada. A validação deve impedir
        /// isto.
        [Fact]
        public void UrlController_ShouldNotCall_ShortenUrl_When_ModelStateIsInvalid()
        {
            var mock = new Mock<IUrlManager>();
            var controller = new UrlController(mock.Object, _mapper);

            controller.ModelState.AddModelError("BadRequest", "No Url Given");
            var result = controller.Shorten(null);

            mock.Verify(urlManager => urlManager.ShortenUrlAsync(null), Times.Never());
        }
    }
}
