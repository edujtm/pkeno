using Xunit;

using Pkeno.Web.Services;

namespace Pkeno.Tests.Di
{
    /// Testes relacionados a injeção de depêndencias.
    ///
    /// Com exceção do primeiro teste, os testes dessa classe
    /// são exemplos de teste reais.
    public class DependencyInjectionTest
    {
        [Fact]
        public void ShouldDeclare_InterfaceWithShorten()
        {
            Assert.Equal("ShortenUrlAsync", nameof(IUrlManager.ShortenUrlAsync));
        }
    }
}
