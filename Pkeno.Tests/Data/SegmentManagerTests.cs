using System.Linq;
using Xunit;
using MockQueryable.Moq;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Data;
using Pkeno.Tests.Utils;

namespace Pkeno.Tests.Data
{
    public class SegmentManagerTests
    {
        private ShortUrl[] samples = {
            new ShortUrl { Id = 0, LongUrl = "https://www.google.com", Segment="1a2b3c", NumOfClicks = 1000 },
            new ShortUrl { Id = 1, LongUrl = "https://www.trovit.com.br", Segment="tr0v1t", NumOfClicks = 10 },
            new ShortUrl { Id = 2, LongUrl = "https://www.vivareal.com.br", Segment="viv4r3al", NumOfClicks = 420 },
        };

        private Stat[] stats = {};

        [Fact(Skip = "Não posso testar isso ainda pois segment manager utiliza Guid que possui comportamento não deterministico.")]
        public void SegmentManager_ShouldRetryOn_DuplicateSegments()
        {
            var mock = samples.AsQueryable().BuildMockDbSet();
            var statsMock = stats.AsQueryable().BuildMockDbSet();

            var context = new TestPkenoAppContext(mock.Object, statsMock.Object);

            var segmentManager = new SegmentManager(context);

            var segment = segmentManager.NewSegment();

            // E o segmento é o que não possui conflito com o banco de dados
            Assert.DoesNotContain(samples, shortUrl => shortUrl.Segment == segment);
        }

    }
}
