using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using MockQueryable.Moq;

using Pkeno.Tests.Utils;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Services;
using Pkeno.Web.Data;

namespace Pkeno.Tests.Data
{
    public class DatabaseTests
    {
        private ShortUrl[] samples = {
            new ShortUrl { Id = 0, LongUrl = "https://www.google.com", Segment="1a2b3c", NumOfClicks = 1000 },
            new ShortUrl { Id = 1, LongUrl = "https://www.trovit.com.br", Segment="tr0v1t", NumOfClicks = 10 },
            new ShortUrl { Id = 2, LongUrl = "https://www.vivareal.com.br", Segment="viv4r3al", NumOfClicks = 420 },
        };

        private Stat[] stats = {};

        [Fact(Skip = "Isso não é responsabilidade do UrlManager e sim do SegmentManager, preciso reorganizar o teste")]
        public async Task UrlManager_ShouldRetryOn_DuplicateSegments()
        {
            var mock = samples.AsQueryable().BuildMockDbSet();
            var statsMock = stats.AsQueryable().BuildMockDbSet();

            var context = new TestPkenoAppContext(mock.Object, statsMock.Object);

            var segmentManagerMock = new Mock<ISegmentManager>();

            segmentManagerMock.SetupSequence<string>(manager => manager.NewSegment())
                .Returns("1a2b3c")
                .Returns("1a2b3c")
                .Returns("1a2b3c")
                .Returns("1a2b3c")
                .Returns("abcdef");

            var url = "https://www.stackoverflow.com/";
            var urlManager = new UrlManager(context, segmentManagerMock.Object);
            var shortUrl = await urlManager.ShortenUrlAsync(url);

            // Checa se o gerenciador de urls tenta gerar novamente um segmento
            // enquanto há conflito.
            segmentManagerMock.Verify(manager => manager.NewSegment(), Times.Exactly(5));
            // E o segmento é o que não possui conflito com o banco de dados
            Assert.Equal("abcdef", shortUrl.Segment);
        }

        [Fact]
        public async Task UrlManager_ShouldReturn_ExistingSegment_When_URLExists()
        {
            var mock = samples.AsQueryable().BuildMockDbSet();
            var statsMock = stats.AsQueryable().BuildMockDbSet();
            var context = new TestPkenoAppContext(mock.Object, statsMock.Object);

            var segmentManagerMock = new Mock<ISegmentManager>();

            var urlManager = new UrlManager(context, segmentManagerMock.Object);

            // Tentando encurtar URL do Google, que já
            // existe no banco de dados
            var shortUrl = await urlManager.ShortenUrlAsync(samples[0].LongUrl); 

            Assert.Equal(samples[0].Id, shortUrl.Id);
            Assert.Equal(samples[0].Segment, shortUrl.Segment);
        }

        [Fact]
        public async Task UrlManager_ShouldThrow_When_UnableToGenerateSegment()
        {
            // Inserindo URL do google no banco de dados
            var mock = samples.AsQueryable().BuildMockDbSet();
            var statsMock = stats.AsQueryable().BuildMockDbSet();
            var context = new TestPkenoAppContext(mock.Object, statsMock.Object);

            var segmentManagerMock = new Mock<ISegmentManager>();
            // SegmentManager retorna uma string vazia quando
            // não consegue gerar um segmento.
            segmentManagerMock.Setup<string>(manager => manager.NewSegment())
                .Returns("");

            var urlManager = new UrlManager(context, segmentManagerMock.Object);

            // Deve gerar uma exceção ArgumentException quando não conseguir 
            // criar um segmento (i.e. SegmentManager retorna uma string vazia)
            await Assert.ThrowsAsync<ArgumentException>(async () => await urlManager.ShortenUrlAsync("http://non-existent.url.on.db/"));
        }
    }
}
