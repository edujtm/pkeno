using System;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;

using Pkeno.Web.Data;
using Pkeno.Web.Services;

namespace Pkeno.Integration.Utils
{
    /// Explicação do teste de integração:
    /// https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-5.0
    /// Outra explicação interessante:
    /// https://raaaimund.github.io/tech/2019/05/08/aspnet-core-integration-testing/
    ///
    /// Não tenho certeza se funciona corretamente
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {

        private readonly DbConnection _connection;

        public CustomWebApplicationFactory()
        {
            _connection = new SqliteConnection("Data Source=:memory:");
            _connection.Open();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services => 
            {
                services.RemoveAll<DbContextOptions<PkenoAppContext>>();
                services.RemoveAll<IPkenoAppContext>();

                services.AddDbContext<PkenoAppContext>(options => 
                {
                    options.UseSqlite(_connection);
                });

                services.AddScoped<IPkenoAppContext, PkenoAppContext>();
                services.AddScoped<IUrlManager, UrlManager>();
                services.AddScoped<ISegmentManager, SegmentManager>();

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;

                    var db = scopedServices.GetRequiredService<PkenoAppContext>();
                    db.Database.EnsureCreated();

                    // Adiciona um usuário de testes no DB
                    scopedServices.UseSeedData();
                    try {
                        Utilities.InitializeDbForTests(db);
                    } 
                    catch (Exception ex)
                    {
                        // Substituir por logger
                        Console.WriteLine($"Error occurred with message: {ex.Message}");
                    }
                }
            });
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _connection.Close();
        }
    }
}
