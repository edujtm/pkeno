using System;
using System.Text;
using System.Text.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Testing;

using Pkeno.Web.Domain.Entities;
using Pkeno.Web.Domain.DTO;
using Pkeno.Web.Data;
using Pkeno.Web.Settings;

namespace Pkeno.Integration.Utils
{
    internal static class Utilities
    {
        public static void InitializeDbForTests(PkenoAppContext context)
        {
            context.ShortUrls.AddRange(GetShortUrls());
            context.SaveChanges();
        }

        public static void ReinitializeDbForTests(PkenoAppContext context)
        {
            context.ShortUrls.RemoveRange(context.ShortUrls);
            context.SaveChanges();
            InitializeDbForTests(context);
        }

        public static List<ShortUrl> GetShortUrls()
        {
            return new List<ShortUrl>()
            {
                new ShortUrl { LongUrl = "https://www.google.com/", Segment="1a2b3c", NumOfClicks = 1000, Added = DateTime.Now },
                new ShortUrl { LongUrl = "https://www.trovit.com.br/", Segment="tr0v1t", NumOfClicks = 10, Added = DateTime.Now },
                new ShortUrl { LongUrl = "https://www.vivareal.com.br/", Segment="viv4r3al", NumOfClicks = 420, Added = DateTime.Now },
            };
        }

        public static JwtSettings JwtSettings { get; } = new JwtSettings {
            Issuer = "http://localhost:5001",
            Key = "TestKey1213123123123123123123123123123"
        };

        public static async Task<HttpClient> LoggedInClient<T>(this WebApplicationFactory<T> factory, string userName, string password) 
            where T : class
        {
            var client = factory.CreateClient(new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false,
                });

            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            var body = JsonSerializer.Serialize(new { username = userName, password = password });
            var postData = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("/api/auth/login", postData);

            var responseBody = await response.Content.ReadAsStreamAsync();
            var token = await JsonSerializer.DeserializeAsync<JwtToken>(responseBody, options);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer", token.Token 
            );
            return client;
        }
    }
}
