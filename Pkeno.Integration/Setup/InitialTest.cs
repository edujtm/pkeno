using System;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

using Pkeno.Web;
using Pkeno.Web.Domain.DTO;

using Pkeno.Integration.Utils;

namespace Pkeno.Integration.Setup
{
    /// Testa apenas a funcionalidade básica do aplicativo app
    /// para passar estes testes não é necessário implementar a conexão
    /// com o banco de dados.
    public class InitialTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {

        private readonly WebApplicationFactory<Startup> _factory;

        public InitialTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        /// Testa se a ação do controlador irá retornar
        /// um modelo correto em JSON.
        [Fact]
        public async Task ShortenAction_ShouldReturn_UrlModel()
        {
            var client = await _factory.LoggedInClient("test", "Test@123");
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            var url = "https://www.google.com/";
            var createUrl = new UrlCreate { Url = url };
            var body = JsonSerializer.Serialize(createUrl);
            var content = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(
                $"/api/urls/shorten",
                content
            );

            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStreamAsync();
            var responseUrl = await JsonSerializer.DeserializeAsync<Url>(responseBody, options);
            Assert.Equal(url, responseUrl.LongURL);
        }

        /// Testa se a URL passada está sendo minimamente
        /// validada. Uma requisição sem especificar a URL
        /// não deve ser aceita.
        ///
        /// Dica: utilizar ModelState e DataAnnotations
        [Theory]
        [InlineData("")]
        [InlineData("invalid_url")]
        [InlineData("http://invalid/")]
        public async Task ShortenAction_UrlParameter_ShouldBe_ValidUrl(string url)
        {
            var client = await _factory.LoggedInClient("test", "Test@123");
            var response = await client.PostAsync("/api/urls/shorten", new StringContent(url, Encoding.UTF8, "application/json"));

            // Recebe um 400 Bad Request por causa que a url
            // não foi informada
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
