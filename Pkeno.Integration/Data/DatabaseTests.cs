using System.Text;
using System.Text.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

using Microsoft.AspNetCore.Mvc.Testing;

using Pkeno.Web;
using Pkeno.Web.Domain.DTO;
using Pkeno.Integration.Utils;

namespace Pkeno.Integration.Data
{
    public class DatabaseTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public DatabaseTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task UrlController_Shorten_ShouldReturn_ExistingSegment()
        {
            var client = await _factory.LoggedInClient("test", "Test@123");
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            var url = "https://www.trovit.com.br/";
            var postData = CreatePostContent(new UrlCreate { Url = url });

            var response = await client.PostAsync(
                "/api/urls/shorten",
                postData
            );
            var responseBody = await response.Content.ReadAsStringAsync();
            var responseUrl = JsonSerializer.Deserialize<Url>(responseBody, options);

            // Data used in the Utilities class
            var urls = Utilities.GetShortUrls();
            var trovitSegment = urls.Find(urlItem => urlItem.LongUrl == url);

            // Then: the returned segmetn should be the one from the database
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(FullUrlForSegment(trovitSegment.Segment), responseUrl.ShortURL);
        }

        private StringContent CreatePostContent<T>(T value)
        {
            var postData = JsonSerializer.Serialize(value);
            return new StringContent(postData, Encoding.UTF8, "application/json"); 
        }

        private string FullUrlForSegment(string segment)
        {
            return $"http://localhost/click/{segment}";
        }
    }
}
